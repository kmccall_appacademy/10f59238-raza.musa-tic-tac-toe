# In your Board class, you should have a grid instance variable to keep track of the board tiles. You should also have the following methods:
#
# place_mark, which takes a position such as [0, 0] and a mark such as :X as arguments. It should throw an error if the position isn't empty.
# empty?, which takes a position as an argument
# winner, which should return a mark
# over?, which should return true or false
# If you want to be a little fancy, read the Bracket Methods reading.
#
#
class Board
  attr_accessor :grid

  def initialize(value = Array.new(3) { Array.new(3)})
    @grid = value
  end

  def place_mark(pos, mark)
    grid[pos.first][pos.last] = mark
  end

  def empty?(pos)
    grid[pos.first][pos.last].nil?
  end

  def winner
    if column_check(grid)
      return column_check(grid)
    elsif row_check(grid)
      return row_check(grid)
    elsif grid[0][0] == :X && grid[1][1] == :X && grid[2][2] == :X
      return :X
    elsif (grid[0][2] == :X && grid[1][1] == :X) && grid[2][0] == :X
      return :X
    end
      nil
  end

  def over?
    grid.flatten.none?(&:nil?) || winner
  end

  private

  def row_check(grid)
    grid.each do |subarray|
      if subarray.all? { |val| val == :X }
        return :X
      end
    end
    false
  end

  def column_check(grid)
    index_num = grid[0].index(:O)
    unless index_num.nil?
      if grid.all? { |subarray| subarray.index(:O) == index_num }
        return :O
      end
    end
    false
  end

end
