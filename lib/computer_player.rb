class ComputerPlayer
  attr_accessor :name, :board, :mark
  def initialize(name)
    @name = name
    @board = board
  end

  def display(board)
    @board = board
  end

  def get_move
    avail_moves = []
    (0..2).each do |column|
      (0..2).each do |row|
        avail_moves << [column, row] if board.grid[column][row].nil?
      end
    end
    avail_moves.each do |array|
      placed_item = board.place_mark(array, :O)
      if board.over?
        board.grid[array.first][array.last] = nil
        return array
      else
        avail_moves.sample
      end
    end
  end
end
